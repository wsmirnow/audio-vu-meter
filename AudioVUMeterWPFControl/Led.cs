﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace AudioVUMeter.Controls
{

    public enum LedStates
    {
        Active = 1,
        Inactive = 0,
    }

    public enum LedColor
    {
        Green = 1,
        Yellow = 2,
        Red = 3,
        Gray = 4,
    }

    [TemplatePart(Name = "PART_LedRectangle", Type = typeof(Rectangle))]
    [TemplateVisualState(Name = "Active", GroupName = "LedStates")]
    [TemplateVisualState(Name = "Inactive", GroupName = "LedStates")]
    [TemplateVisualState(Name = "Green", GroupName = "LedColor")]
    [TemplateVisualState(Name = "Yellow", GroupName = "LedColor")]
    [TemplateVisualState(Name = "Red", GroupName = "LedColor")]
    [TemplateVisualState(Name = "Gray", GroupName = "LedColor")]
    public class Led : Control
    {
        static Led()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Led), new FrameworkPropertyMetadata(typeof(Led)));
        }

        public Led()
        {
            IsTabStop = false;
            Focusable = false;
        }

        #region Properties

        #region State Property

        public LedStates State
        {
            get { return (LedStates)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register(
                "State",
                typeof(LedStates),
                typeof(Led),
                new PropertyMetadata(LedStates.Inactive, OnLedStateChanged)
            );

        private static void OnLedStateChanged(DependencyObject d,
                 DependencyPropertyChangedEventArgs e)
        {
            ((Led)d).SetState();
        }

        private void SetState()
        {
            switch (State)
            {
                case LedStates.Active:
                    VisualStateManager.GoToState(this, "Active", true);
                    break;
                case LedStates.Inactive:
                    VisualStateManager.GoToState(this, "Inactive", true);
                    break;
                default:
                    VisualStateManager.GoToState(this, "Inactive", true);
                    break;
            }
        }
        #endregion

        #region Color Property
        public LedColor Color
        {
            get { return (LedColor)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register(
                "Color",
                typeof(LedColor),
                typeof(Led),
                new PropertyMetadata(LedColor.Green, OnLedValueStateChanged)
            );

        private static void OnLedValueStateChanged(DependencyObject d,
                 DependencyPropertyChangedEventArgs e)
        {
            ((Led)d).SetColor();
        }

        private void SetColor()
        {
            if (!IsEnabled)
            {
                VisualStateManager.GoToState(this, "Gray", true);
                return;
            }

            switch (Color)
            {
                case LedColor.Yellow:
                    VisualStateManager.GoToState(this, "Yellow", true);
                    break;
                case LedColor.Red:
                    VisualStateManager.GoToState(this, "Red", true);
                    break;
                case LedColor.Gray:
                    VisualStateManager.GoToState(this, "Gray", true);
                    break;
                case LedColor.Green:
                default:
                    VisualStateManager.GoToState(this, "Green", true);
                    break;
            }
        }
        #endregion

        #region MinValue Property
        public double MinValue
        {
            get { return (double)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register(
                "MinValue",
                typeof(double),
                typeof(Led),
                new PropertyMetadata(0.0, OnMinValueChanged)
            );

        private static void OnMinValueChanged(DependencyObject d,
                 DependencyPropertyChangedEventArgs e)
        {
            ((Led)d).SetMinValue();
        }

        private void SetMinValue()
        {
            SetValue();
        }
        #endregion

        #region Value Property
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
                "Value",
                typeof(double),
                typeof(Led),
                new PropertyMetadata(-1.0, OnValueChanged)
            );

        private static void OnValueChanged(DependencyObject d,
                 DependencyPropertyChangedEventArgs e)
        {
            ((Led)d).SetValue();
        }

        private void SetValue()
        {
            if (Value > MinValue)
            {
                if (State != LedStates.Active)
                    State = LedStates.Active;
            }
            else
            {
                if (State != LedStates.Inactive)
                    State = LedStates.Inactive;
            }
        }
        #endregion

        #endregion

        public override void OnApplyTemplate()
        {
            SetState();
            SetColor();
            SetValue();
            base.OnApplyTemplate();
            IsEnabledChanged += (s, e) => { SetColor(); };
        }
    }
}
