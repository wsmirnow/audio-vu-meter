﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace AudioVUMeter.Controls
{
    public enum VUOrientation
    {
        LeftToRight,
        RightToLeft,
        TopToBottom,
        BottomToTop,
    }

    [TemplatePart(Name = "PART_VUMeterBorder", Type = typeof(Border))]
    [TemplatePart(Name = "PART_VUMeterChannelsGrid", Type = typeof(UniformGrid))]
    public class AudioVUMeter : RangeBase, IVUCallback
    {
        static AudioVUMeter()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AudioVUMeter), new FrameworkPropertyMetadata(typeof(AudioVUMeter)));
        }

        public AudioVUMeter()
        {
            IsTabStop = false;
            Focusable = false;
        }

        #region Properties

        #region VUOrientation Property

        public VUOrientation VUOrientation
        {
            get { return (VUOrientation)GetValue(VUOrientationProperty); }
            set { SetValue(VUOrientationProperty, value); }
        }

        public static readonly DependencyProperty VUOrientationProperty =
            DependencyProperty.Register(
                "VUOrientation",
                typeof(VUOrientation),
                typeof(AudioVUMeter),
                new PropertyMetadata(VUOrientation.LeftToRight, OnVUOrientationChanged)
            );

        private static void OnVUOrientationChanged(DependencyObject d, DependencyPropertyChangedEventArgs value)
        {
            AudioVUMeter AudioVUMeter = (AudioVUMeter)d;
            if (AudioVUMeter != null)
                AudioVUMeter.SetVUOrientation();
        }

        protected void SetVUOrientation()
        {
            UniformGrid VUMeterChannelsGrid = GetTemplateChild("PART_VUMeterChannelsGrid") as UniformGrid;
            if (VUMeterChannelsGrid != null)
            {
                switch (VUOrientation)
                {
                    case VUOrientation.TopToBottom:
                    case VUOrientation.BottomToTop:
                        VUMeterChannelsGrid.Rows = 1;
                        VUMeterChannelsGrid.Columns = 2;
                        break;
                    case VUOrientation.LeftToRight:
                    case VUOrientation.RightToLeft:
                    default:
                        VUMeterChannelsGrid.Rows = 2;
                        VUMeterChannelsGrid.Columns = 1;
                        break;
                }
            }
        }

        #endregion

        #endregion

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            SetVUOrientation();
        }

        #region IVUCallback Implementation

        public void NewSampleValues(int nChannels, VUValue[] Values)
        {
            if (nChannels < 1) return;
            SetSampleValues(Values);
        }

        private void SetSampleValues(VUValue[] Values)
        {
            AudioChannelVUMeter ChannelMeter = null;
            ChannelMeter = GetTemplateChild("PART_VUMeterChannel01") as AudioChannelVUMeter;
            if (ChannelMeter != null)
            {
                SetValueOnRangeBase(ChannelMeter, Values[0].Volume);
            }
            ChannelMeter = GetTemplateChild("PART_VUMeterChannel02") as AudioChannelVUMeter;
            if (ChannelMeter != null)
            {
                SetValueOnRangeBase(ChannelMeter, Values[Values.Length == 1 ? 0 : 1].Volume);
            }
        }

        private static void SetValueOnRangeBase(RangeBase AudioChannelVUMeter, double Value)
        {
            if (!AudioChannelVUMeter.CheckAccess())
            {
                AudioChannelVUMeter.Dispatcher.BeginInvoke(
                    new Action<RangeBase, double>(SetValueOnRangeBase), 
                    System.Windows.Threading.DispatcherPriority.DataBind,
                    AudioChannelVUMeter, Value);
                return;
            }
            if (Value < AudioChannelVUMeter.Minimum)
                Value = AudioChannelVUMeter.Minimum;
            if (Value > AudioChannelVUMeter.Maximum)
                Value = AudioChannelVUMeter.Maximum;
            AudioChannelVUMeter.Value = Value;
        }

        #endregion
    }
}
