﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace AudioVUMeter
{
    [ComImport, Guid("8E9520D8-8606-465C-BF78-C0E11F3E7EBA")]
    public class AudioVUMeterSinkFilter { }

    [StructLayout(LayoutKind.Sequential)]
    public struct VUValue
    {
        public Int32 RMS;
        public float Volume;

        public static VUValue Zero
        {
            get { return new VUValue(); }
        }

        public VUValue(Int32 RMS = 0, float Volume = 0.0f)
        {
            this.RMS = RMS;
            this.Volume = Volume;
        }

        public override string ToString()
        {
            return string.Format("[{0} {1}]", RMS, Volume);
        }
    };

    [ComVisible(true), ComImport, SuppressUnmanagedCodeSecurity,
    Guid("C9CD7C58-990B-4430-BD60-28537448D960"),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IVUCallback
    {
        void NewSampleValues(
            Int32 nChannels, 
            [MarshalAs(UnmanagedType.LPArray, SizeParamIndex=0)] VUValue[] pValues);
    }

    [ComVisible(true), ComImport, SuppressUnmanagedCodeSecurity,
    Guid("4938C3DB-6088-45ED-BF94-62A24ACA84C2"),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IVURegisterCallback
    {
        void SetVUCallback(IVUCallback pVUCallback);
    }

}
