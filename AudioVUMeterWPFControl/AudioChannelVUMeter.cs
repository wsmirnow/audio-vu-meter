﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace AudioVUMeter.Controls
{
    [TemplatePart(Name = "PART_AudioChannelBorder", Type = typeof(Border))]
    [TemplatePart(Name = "PART_AudioChannelUGrid", Type = typeof(UniformGrid))]
    public class AudioChannelVUMeter : RangeBase
    {
        static readonly int LedsPerChannel = 15;

        static AudioChannelVUMeter()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(AudioChannelVUMeter), new FrameworkPropertyMetadata(typeof(AudioChannelVUMeter)));
        }

        public AudioChannelVUMeter()
        {
            IsTabStop = false;
            Focusable = false;
        }

        #region Properties

        #region VUOrientation Property

        public VUOrientation VUOrientation
        {
            get { return (VUOrientation)GetValue(VUOrientationProperty); }
            set { SetValue(VUOrientationProperty, value); }
        }

        public static readonly DependencyProperty VUOrientationProperty =
            DependencyProperty.Register(
                "VUOrientation",
                typeof(VUOrientation),
                typeof(AudioChannelVUMeter),
                new PropertyMetadata(VUOrientation.LeftToRight, OnVUOrientationChanged)
            );

        private static void OnVUOrientationChanged(DependencyObject d, DependencyPropertyChangedEventArgs value)
        {
            AudioChannelVUMeter AudioChannelVUMeter = (AudioChannelVUMeter)d;
            if (AudioChannelVUMeter != null)
                AudioChannelVUMeter.RebuildLedGrid();
        }

        #endregion

        #endregion

        protected void RebuildLedGrid()
        {
            RebuildLedGrid(Minimum, Maximum);
        }

        protected void RebuildLedGrid(double MinValue, double MaxValue)
        {
            if (Template == null) return;

            UniformGrid LedsUGrid = GetTemplateChild("PART_AudioChannelUGrid") as UniformGrid;
            if (LedsUGrid != null)
            {
                LedsUGrid.Children.Clear();
                switch (VUOrientation)
                {
                    case VUOrientation.TopToBottom:
                    case VUOrientation.BottomToTop:
                        LedsUGrid.Rows = LedsPerChannel;
                        LedsUGrid.Columns = 1;
                        break;
                    case VUOrientation.RightToLeft:
                    case VUOrientation.LeftToRight:
                    default:
                        LedsUGrid.Rows = 1;
                        LedsUGrid.Columns = LedsPerChannel;
                        break;
                }
                
                for (int LedIdx = 0; LedIdx < LedsPerChannel; LedIdx++)
                {
                    double LedMinValue = MinValue;
                    LedMinValue += ((MaxValue - MinValue) / LedsPerChannel * LedIdx);
                    Led Led = new Led() 
                    { 
                        Name = string.Format("PART_Led{0:00}", LedIdx),
                        MinValue = LedMinValue,
                        Color = LedColorGenerator(LedIdx, LedsPerChannel),
                    };
                    Binding ValueBinding = new Binding() { 
                        Source = this,
                        Path = new PropertyPath("Value"),
                        Mode = BindingMode.OneWay,
                    };
                    Led.SetBinding(Led.ValueProperty, ValueBinding);
                    Binding IsEnabledBinding = new Binding()
                    {
                        Source = this,
                        Path = new PropertyPath("IsEnabled"),
                        Mode = BindingMode.OneWay,
                    };
                    Led.SetBinding(Led.IsEnabledProperty, IsEnabledBinding);

                    if (VUOrientation == Controls.VUOrientation.LeftToRight ||
                        VUOrientation == Controls.VUOrientation.TopToBottom)
                    {
                        LedsUGrid.Children.Insert(LedIdx, Led);
                    }
                    else
                    {
                        LedsUGrid.Children.Insert(0, Led);
                    }
                }
            }
        }

        protected static LedColor LedColorGenerator(int LedIndex, int nLeds)
        {
            if ((nLeds * 0.9) <= LedIndex)
            {
                return LedColor.Red;
            }
            else if ((nLeds * 0.7) <= LedIndex)
            {
                return LedColor.Yellow;
            } 
            else
            {
                return LedColor.Green;
            }
        }

        protected override void OnMaximumChanged(double oldMaximum, double newMaximum)
        {
            base.OnMinimumChanged(oldMaximum, newMaximum);
            RebuildLedGrid(Minimum, newMaximum);
        }
        
        protected override void OnMinimumChanged(double oldMinimum, double newMinimum)
        {
            base.OnMinimumChanged(oldMinimum, newMinimum);
            RebuildLedGrid(newMinimum, Maximum);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            RebuildLedGrid(Minimum, Maximum);
        }
    }
}
