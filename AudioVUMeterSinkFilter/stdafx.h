#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <tchar.h>

#include <streams.h>
#include <initguid.h>

#define SAFE_RELEASE(pUnkn) if (pUnkn) { pUnkn->Release(); pUnkn = NULL; }

#define IF_FAILED_GOTO(hr, label) if (FAILED(hr)) { goto label; }
#define CHECK_HR(hr) IF_FAILED_GOTO(hr, done)

