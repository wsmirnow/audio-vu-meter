#include "PCMParser.h"

HRESULT ParseU8(LPBYTE pBuffer, UINT8 *pValue)
{
	CheckPointer(pBuffer, E_POINTER);
	CheckPointer(pValue, E_POINTER);

	*pValue = 0;
	*pValue = pBuffer[0];
	return S_OK;
}

HRESULT ParseI8(LPBYTE pBuffer, INT16 *pValue)
{
	return ParseU8(pBuffer, (UINT8 *)pValue);
}

HRESULT ParseU16(LPBYTE pBuffer, UINT16 *pValue)
{
	CheckPointer(pBuffer, E_POINTER);
	CheckPointer(pValue, E_POINTER);

	*pValue = 0;
	*pValue = pBuffer[0] | pBuffer[1] << 8;

	return S_OK;
}

HRESULT ParseI16(LPBYTE pBuffer, INT16 *pValue)
{
	return ParseU16(pBuffer, (UINT16 *)pValue);
}

HRESULT ParseU24(LPBYTE pBuffer, UINT32 *pValue)
{
	CheckPointer(pBuffer, E_POINTER);
	CheckPointer(pValue, E_POINTER);

	*pValue = 0;
	*pValue = pBuffer[0] | pBuffer[1] << 8 | pBuffer[2] << 16;
	return S_OK;
}

HRESULT ParseI24(LPBYTE pBuffer, INT32 *pValue)
{
	HRESULT hr = S_OK;
	
	CHECK_HR(hr = ParseU24(pBuffer, (UINT32 *)pValue))
	if (*pValue & (1 << 23))
	{
		*pValue |= (0xFF << 24);
	}

done:
	return hr;
}

HRESULT ParseU32(LPBYTE pBuffer, UINT32 *pValue)
{
	CheckPointer(pBuffer, E_POINTER);
	CheckPointer(pValue, E_POINTER);

	*pValue = 0;
	*pValue = pBuffer[0] | pBuffer[1] << 8 | pBuffer[2] << 16 | pBuffer[3] << 24;
	return S_OK;
}

HRESULT ParseI32(LPBYTE pBuffer, INT32 *pValue)
{
	return ParseU32(pBuffer, (UINT32 *)pValue);
}

HRESULT ParseSamples(WORD nChannels, WORD BitPerSample, DWORD cBuffer, LPBYTE pBuffer, VUVALUES *ppVUValues)
{
	HRESULT hr = S_OK;

	DWORD cSample;
	WORD cChannel;
	LPBYTE pSample;
	INT32 ChannelSampleValue;
	DOUBLE *TotalSquare = NULL;
	UINT32 MaxValue = 0;
	INT32 nSamples = 0;
	UINT8 u8V;
	INT16 i16V;
	INT32 i32V;
	
	if (nChannels == 0) return E_INVALIDARG;
	if (BitPerSample == 0 || BitPerSample % 8 != 0) return E_INVALIDARG;
	// at least one sample per channel should be provided
	if ((LONGLONG)cBuffer < (INT32)(nChannels * BitPerSample)) return E_INVALIDARG; 
	CheckPointer(pBuffer, E_POINTER);
	CheckPointer(ppVUValues, E_POINTER);

	switch (BitPerSample)
	{
	case 8:
		MaxValue = (UINT32)(UINT8_MAX);
		break;
	case 16:
		MaxValue = (UINT32)(((UINT32)INT16_MAX) + 1);
		break;
	case 24:
		MaxValue = (UINT32)(0x800000);
		break;
	case 32:
		MaxValue = (UINT32)(((UINT32)INT32_MAX) + 1);
		break;
	default:
		hr = E_INVALIDARG;
		goto done;
	}
	nSamples = (INT32)((FLOAT)cBuffer * 8.0f / (FLOAT)(BitPerSample * nChannels));

	TotalSquare = new DOUBLE[nChannels];
	for (cSample = 0; (LONGLONG)cSample < nSamples; cSample++)
	{
		// for each sample
		pSample = pBuffer + (cSample * nChannels * BitPerSample / 8);
		// assert you are in buffer range for next sample
		ASSERT((pSample + (nChannels * BitPerSample / 8)) <= (pBuffer + cBuffer));
		for (cChannel = 0; cChannel < nChannels; cChannel++)
		{
			// for each channel
			if (cSample == 0) TotalSquare[cChannel] = 0;

			ChannelSampleValue = 0;
			switch (BitPerSample)
			{
			case 8:
				u8V = 0;
				CHECK_HR(hr = ParseU8(pSample, &u8V))
				ChannelSampleValue = u8V;
				break;
			case 16:
				i16V = 0;
				CHECK_HR(hr = ParseI16(pSample, &i16V))
				ChannelSampleValue = i16V;
				break;
			case 24:
				i32V = 0;
				CHECK_HR(hr = ParseI24(pSample, &i32V))
				ChannelSampleValue = i32V;
				break;
			case 32:
				i32V = 0;
				CHECK_HR(hr = ParseI32(pSample, &i32V))
				ChannelSampleValue = i32V;
				break;
			default:
				hr = E_INVALIDARG;
				goto done;
			}

			TotalSquare[cChannel] += ((DOUBLE)pow((DOUBLE)ChannelSampleValue, 2.0) / (DOUBLE)nSamples);
			ASSERT(TotalSquare[cChannel] < HUGE_VAL && TotalSquare[cChannel] >= 0);
		}
	}

	for (cChannel = 0; cChannel < nChannels; cChannel++)
	{
		ASSERT(TotalSquare[cChannel] >= 0);
		if (TotalSquare[cChannel] < 0)
		{
			hr = E_FAIL;
			goto done;
		}
		DOUBLE RMS = sqrt((DOUBLE)TotalSquare[cChannel]);
		(*ppVUValues)[cChannel].RMS = (INT32)round(RMS);
		(*ppVUValues)[cChannel].Volume = (FLOAT)(RMS / (DOUBLE)MaxValue);
	}

done:
	if (TotalSquare)
	{
		delete[] TotalSquare;
	}

	return hr;
}