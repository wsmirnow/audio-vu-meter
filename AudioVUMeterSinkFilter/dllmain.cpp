#include "stdafx.h"
#include "AudioVUMeterSinkFilter.h"

#pragma comment(lib, "winmm")
#ifdef _DEBUG
#pragma comment(lib, "strmbasd")
#pragma comment(lib, "msvcrtd")
#else
#pragma comment(lib, "strmbase")
#pragma comment(lib, "msvcrt")
#endif

const AMOVIESETUP_MEDIATYPE pinTypes[1] =
{
	{
		&MEDIATYPE_Audio,
		&MEDIASUBTYPE_PCM
	},
};

const AMOVIESETUP_PIN inputPin =
{
	_T(PIN_NAME),
	FALSE,                              // Is it rendered
	FALSE,                              // Is it an output
	FALSE,                              // Allowed zero pins
	FALSE,                              // Allowed many
	&CLSID_NULL,                        // Connects to filter
	L"Output",                          // Connects to pin
	sizeof(pinTypes) / sizeof(pinTypes[0]), // Number of pins types
	pinTypes                            // Pin information
};

const AMOVIESETUP_FILTER sinkFilter =
{
	&CLSID_AudioVUMeterSinkFilter,
	_T(FILTER_NAME),
	MERIT_DO_NOT_USE,
	1,
	&inputPin
};

CFactoryTemplate g_Templates[1] =
{
	{
		_T(FILTER_NAME),
		&CLSID_AudioVUMeterSinkFilter,
		AudioVUMeterSinkFilter::CreateInstance,
		NULL,
		&sinkFilter
	}
};
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


STDAPI DllRegisterServer()
{
	return AMovieDllRegisterServer2(TRUE);
}

STDAPI DllUnregisterServer()
{
	return AMovieDllRegisterServer2(FALSE);
}

extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);
BOOL APIENTRY DllMain(HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}
