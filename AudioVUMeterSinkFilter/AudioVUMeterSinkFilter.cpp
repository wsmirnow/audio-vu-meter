#include "stdafx.h"
#include "AudioVUMeterSinkFilter.h"

HRESULT AudioVUMeterSinkFilter::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IAMFilterMiscFlags)
	{
		return  GetInterface((IAMFilterMiscFlags *)this, ppv);
	}
	if (riid == __uuidof(IVURegisterCallback))
	{
		return GetInterface((IVURegisterCallback *)this, ppv);
	}
	return CBaseFilter::NonDelegatingQueryInterface(riid, ppv);
}

CUnknown *AudioVUMeterSinkFilter::CreateInstance(LPUNKNOWN pUnkn, HRESULT *phr)
{
	AudioVUMeterSinkFilter *pNewObject = new AudioVUMeterSinkFilter(pUnkn, phr);
	if (!pNewObject) {
		*phr = E_OUTOFMEMORY;
	}
	return pNewObject;
}

AudioVUMeterSinkFilter::AudioVUMeterSinkFilter(LPUNKNOWN pUnkn, HRESULT *phr)
	: CBaseFilter(NAME(FILTER_NAME), pUnkn, this, CLSID_AudioVUMeterSinkFilter)
{
	if (FAILED(*phr)) return;

	m_pPin = new AudioVUMeterInputPin(this, this, phr);
}

AudioVUMeterSinkFilter::~AudioVUMeterSinkFilter()
{
	if (m_pPin)
	{
		delete m_pPin;
		m_pPin = NULL;
	}
}

VOID AudioVUMeterSinkFilter::SetVUCallback(IVUCallback *pVUCallback)
{
	if (m_pPin)
	{
		m_pPin->SetVUCallback(pVUCallback);
	}
}