#pragma once
#include "stdafx.h"
#include "VUStructs.h"


MIDL_INTERFACE("C9CD7C58-990B-4430-BD60-28537448D960")
IVUCallback : public IUnknown
{
public:
	virtual VOID STDAPICALLTYPE NewSampleValues(INT nChannels, VUVALUES pValues) = 0;
};

MIDL_INTERFACE("4938C3DB-6088-45ED-BF94-62A24ACA84C2")
IVURegisterCallback : IUnknown
{
public:
	virtual VOID STDAPICALLTYPE SetVUCallback(IVUCallback *pVUCallback) = 0;
};
