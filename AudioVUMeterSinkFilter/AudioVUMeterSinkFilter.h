#pragma once

#include "stdafx.h"
#include "FilterCallbacks.h"


// {8E9520D8-8606-465C-BF78-C0E11F3E7EBA}
DEFINE_GUID(CLSID_AudioVUMeterSinkFilter,
	0x8e9520d8, 0x8606, 0x465c, 0xbf, 0x78, 0xc0, 0xe1, 0x1f, 0x3e, 0x7e, 0xba);

#define FILTER_NAME "Audio VU Meter"
#define PIN_NAME "Input"

class AudioVUMeterInputPin : public CBaseInputPin
{
	friend class AudioVUMeterSinkFilter;

public:
	DECLARE_IUNKNOWN

	virtual HRESULT CheckMediaType(const CMediaType *);
	virtual HRESULT GetMediaType(int iPosition, CMediaType *);
	virtual HRESULT SetMediaType(const CMediaType *);
	virtual STDMETHODIMP Receive(IMediaSample *);
	virtual HRESULT Inactive(void);

	VOID STDAPICALLTYPE SetVUCallback(IVUCallback *);

private:
	IVUCallback *m_pVUCallback;
	CCritSec *m_pVUCallbackLock;

	WORD m_nChannels;
	WORD m_wBitsPerSample;

	DECLSPEC_NOTHROW AudioVUMeterInputPin(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr);
	~AudioVUMeterInputPin();
};

class AudioVUMeterSinkFilter : public CBaseFilter, CCritSec, public IAMFilterMiscFlags, public IVURegisterCallback
{
public:
	DECLARE_IUNKNOWN
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void **ppv);

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN pUnkn, HRESULT *phr);

	// CBaseFilter section
	int GetPinCount() { return 1; }
	CBasePin *GetPin(int n) { return n == 0 ? reinterpret_cast<CBasePin *>(m_pPin) : NULL; }

	ULONG STDMETHODCALLTYPE GetMiscFlags(void)
	{
		return AM_FILTER_MISC_FLAGS_IS_RENDERER;
	}

	// IVURegisterCallback section
	VOID STDAPICALLTYPE SetVUCallback(IVUCallback *pVUCallback);


private:
	AudioVUMeterInputPin *m_pPin;

	DECLSPEC_NOTHROW AudioVUMeterSinkFilter(LPUNKNOWN pUnkn, HRESULT *phr);
	~AudioVUMeterSinkFilter();
};
