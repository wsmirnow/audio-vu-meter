#include "stdafx.h"
#include "AudioVUMeterSinkFilter.h"
#include "PCMParser.h"


AudioVUMeterInputPin::AudioVUMeterInputPin(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr)
	: CBaseInputPin(NAME(PIN_NAME), pFilter, pLock, phr, _T(PIN_NAME)), 
	m_pVUCallback(NULL), m_pVUCallbackLock()
{

}

AudioVUMeterInputPin::~AudioVUMeterInputPin()
{
	if (m_pVUCallback)
	{
		SetVUCallback(NULL);
	}

	if (m_pVUCallbackLock)
	{
		delete m_pVUCallbackLock;
		m_pVUCallbackLock = NULL;
	}
}

HRESULT AudioVUMeterInputPin::CheckMediaType(const CMediaType *pmt)
{
	if (pmt->majortype == MEDIATYPE_Audio)
	{
		if (pmt->subtype == MEDIASUBTYPE_PCM &&
			pmt->formattype == FORMAT_WaveFormatEx &&
			pmt->bTemporalCompression == FALSE)
		{
			return S_OK;
		}
	}
	return S_FALSE;
}

HRESULT AudioVUMeterInputPin::GetMediaType(int iPosition, CMediaType *pMediaType)
{
	if (iPosition > 0) return VFW_S_NO_MORE_ITEMS;
	if (iPosition > 0) return E_INVALIDARG;
	CheckPointer(pMediaType, E_POINTER);
	return pMediaType->Set(m_mt);
}

HRESULT AudioVUMeterInputPin::SetMediaType(const CMediaType *pMediaType)
{
	HRESULT hr = S_OK;
	WAVEFORMATEX *wf = NULL;

	CHECK_HR(hr = CBaseInputPin::SetMediaType(pMediaType))
	
	if (pMediaType->formattype == FORMAT_WaveFormatEx && pMediaType->cbFormat >= sizeof(WAVEFORMAT))
	{
		wf = (WAVEFORMATEX *)pMediaType->pbFormat;
		m_nChannels = wf->nChannels;
		m_wBitsPerSample = wf->wBitsPerSample;
	}
	else 
	{
		m_nChannels = 0;
		m_wBitsPerSample = 0;
	}
done:
	return hr;
}

#pragma warning(push)
#pragma warning(disable:4101)
HRESULT AudioVUMeterInputPin::Receive(IMediaSample *pSample)
{
	HRESULT hr = S_OK;
	VUValue *pVUValues = NULL;

	CHECK_HR(hr = CBaseInputPin::Receive(pSample));
	NOTE2("Sample recieved at %lld - %lld", m_SampleProps.tStart, m_SampleProps.tStop);
	
	if (m_nChannels && m_wBitsPerSample) 
	{
		pVUValues = new VUValue[m_nChannels];

		// parse audio data
		CHECK_HR(hr = ParseSamples(m_nChannels, m_wBitsPerSample, m_SampleProps.cbBuffer, m_SampleProps.pbBuffer, &pVUValues));

		// send vu values through caalback
		NOTE3("VU value: RMS %d Volume %f dB %f", pVUValues[0].RMS, pVUValues[0].Volume, DB_FROM_VALUE((DOUBLE)pVUValues[0].RMS));
		{
			CAutoLock(*m_pVUCallbackLock);

			if (m_pVUCallback)
			{
				m_pVUCallback->NewSampleValues(m_nChannels, pVUValues);
			}
			//Sleep(80);
		}
	}

done:
	return hr;
}
#pragma warning(pop)


#pragma warning(push)
#pragma warning(disable:4101)
HRESULT AudioVUMeterInputPin::Inactive(void) 
{
	VUValue *pVUValues = NULL;
	NOTE("Inactive called on AudioVUMeterInputPin");
	HRESULT hr = CBasePin::Inactive();

	CAutoLock(*m_pVUCallbackLock);
	if (m_pVUCallback)
	{
		pVUValues = new VUValue[m_nChannels];
		m_pVUCallback->NewSampleValues(m_nChannels, pVUValues);
	}
	return hr;
}
#pragma warning(pop)


#pragma warning(push)
#pragma warning(disable:4101)
VOID AudioVUMeterInputPin::SetVUCallback(IVUCallback *pVUCallback)
{
	CAutoLock(*m_pVUCallbackLock);

	m_pVUCallback = pVUCallback;
}
#pragma warning(pop)