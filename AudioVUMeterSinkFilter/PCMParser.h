#pragma once
#include "stdafx.h"
#include "VUStructs.h"
#include <math.h>


#define DB_FROM_VALUE(value) value >= 1 ? 20*log10(value) : 0

STDMETHODIMP ParseSamples(WORD nChannels, WORD BitPerSample, DWORD cBuffer, LPBYTE pBuffer, VUVALUES *ppVUValues);